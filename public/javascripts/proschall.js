// function createMatchInputs() {
//     const addButton = document.querySelector(".btn-add");
//     const input = document.querySelector(".add-input");
//     let $list = input.closest(".list");
//     let inputs = $list.getElementsByTagName("div");
//     addButton.onclick = function (event) {
//         event.preventDefault();
//         if (input.value.trim() != "") {
//             [...inputs];
//             let div = document.createElement("div");
//             div.innerText = input.value;
//             $list.prepend(div);
//             input.value = "";
//             inputs = inputs;
//         }
//     }
//     for (let i = 0; i < inputs.length; i++) {
//         inputs[i].onclick = function () {
//             const $inputFields = this.closest(".input-field");
//             let $currentTxt = $inputFields.children[1];
//             let $list = $inputFields.lastElementChild;
//             let $selectedItem = $list.querySelectorAll(".selected");
//             if (this.classList.length == 0) {
//                 for (let j = 0; j < $selectedItem.length; j++) {
//                     $selectedItem[j].classList.remove("selected");
//                 }
//                 this.classList.add("selected");
//                 $currentTxt.innerHTML = this.innerHTML;
//                 $list.classList.remove("active");
//                 $currentTxt.classList.remove("active");
//             }
//         };
//     }
// }



$(document).ready(function () {
    $("header .menu-btn").click(function () {
        var $this = $(this);
        if (!$this.hasClass("active")) {
            $this.addClass("active");
            $(".left-navigation").addClass("open");
            $("header").addClass("active");
            $(".logo-area .small-logo").addClass("d-none");
            $(".logo-area .big-logo").removeClass("d-none");
            setTimeout(function () {
                $(".left-navigation .games-list .item .name").fadeIn(100);
                $(".left-navigation .navigation ul").removeClass("d-none");
                $(".left-navigation  .bottom-info .info-inner").removeClass("d-none");
            }, 200);
            $("header .layer").removeClass("d-none");
        }
        else {
            $this.removeClass("active");
            $(".left-navigation").removeClass("open");
            $("header").removeClass("active");
            $(".logo-area .small-logo").removeClass("d-none");
            $(".logo-area .big-logo").addClass("d-none");
            $(".left-navigation .games-list .item .name").fadeOut(0);
            $(".left-navigation .navigation ul").addClass("d-none");
            $(".left-navigation .bottom-info .info-inner").addClass("d-none");
            $("header .layer").addClass("d-none");
        }
    });

    $(".left-navigation .games-list .item").click(function () {
        var $this = $(this);
        if (!$this.hasClass("active")) {
            $(".left-navigation .games-list .item.active").removeClass("active");
            $this.addClass("active");
        }
    });

    $("header .buttons-area .user-info-area .username-btn").click(function () {
        var $this = $(this);
        var $popup = $this.closest(".user-info-area").find(".user-info");
        var $triangle = $this.closest(".user-info-area").find(".triangle");
        if (!$this.hasClass("active")) {
            if ($("header .buttons-area .notification-btn.active").length > 0) {
                $("header .buttons-area .triangle").fadeOut(300);
                $("header .buttons-area .notifications.active").removeClass("active").fadeOut(300);
                $("header .buttons-area .notification-btn.active").removeClass("active");
                $("header .buttons-area .notification-area .n-list .item.active .notification.active").fadeOut(300).removeClass("active");
                $("header .buttons-area .notification-area .n-list .item.active").removeClass("active");
            }
            $this.addClass("active");
            $popup.addClass("active").fadeIn(300);
            $triangle.fadeIn(300);
        }
        else {
            $triangle.fadeOut(300);
            $popup.removeClass("active").fadeOut(300);
            $this.removeClass("active");
        }
    });

    $("header .buttons-area .notification-btn").click(function () {
        var $this = $(this);
        var $notifications = $this.closest(".notification-area").find(".notifications");
        var $triangle = $this.closest(".notification-area").find(".triangle");
        if (!$this.hasClass("active")) {
            if ($("header .buttons-area .user-info-area .username-btn.active").length > 0) {
                $("header .buttons-area .user-info-area .triangle").fadeOut(300);
                $("header .buttons-area .user-info-area .user-info.active").removeClass("active").fadeOut(300);
                $("header .buttons-area .user-info-area .username-btn").removeClass("active");
            }
            $this.addClass("active");
            $notifications.addClass("active").fadeIn(300);
            $triangle.fadeIn(300);
        }
        else {
            $triangle.fadeOut(300);
            $notifications.removeClass("active").fadeOut(300);
            $this.removeClass("active");
            $("header .buttons-area .notification-area .n-list .item.active .notification.active").fadeOut(300).removeClass("active");
            $("header .buttons-area .notification-area .n-list .item.active").removeClass("active");
        }
    });

    $("header .buttons-area .notification-area .n-list .item").click(function (e) {
        var $this = $(this);
        var $notification = $this.find(".notification");
        if (!$this.hasClass("active")) {
            $("header .buttons-area .notification-area .n-list .item.active .notification.active").fadeOut(300).removeClass("active");
            $("header .buttons-area .notification-area .n-list .item.active").removeClass("active");
            $this.addClass("active");
            $notification.fadeIn(300).addClass("active");
        }
        else {
            if ($(e.target).closest(".notification.active").length === 0) {
                $notification.fadeOut(300).removeClass("active");
                $this.removeClass("active");
            }
        }
    });

    $(".questions-section .questions-list .item .tab-header a").click(function () {
        var $item = $(this).closest(".item");
        var $text = $item.find(".tab-content");
        if (!$item.hasClass("active")) {
            $(".questions-section .questions-list .item.active .tab-content").slideUp(300);
            $(".questions-section .questions-list .item.active").removeClass("active");
            $item.addClass("active");
            $text.slideDown(300);
        }
        else {
            $text.slideUp(300);
            $item.removeClass("active");
        }
    });

    $(".popup-outer .tab-header .tab-link").click(function () {
        var $this = $(this);
        if (!$this.hasClass("active")) {
            $(".popup-outer .tab-header .tab-link.active").removeClass("active");
            $(".popup-outer .tab-item.active").removeClass("active");
            $this.addClass("active");
            $(".popup-outer .tab-item[data-tab='" + $this.attr("data-tab") + "']").addClass("active");
        }
    });

    $("header .buttons-area.logout a, .sign-btn").click(function () {
        var $this = $(this);
        $(".popup-outer .tab-item[data-tab='" + $this.attr("data-tab") + "']").addClass("active");
        $(".popup-outer .tab-link[data-tab='" + $this.attr("data-tab") + "']").addClass("active");
        $(".popup-outer").fadeIn(300);
        $(".popup-outer").addClass("active")
    });

    $(".popup-outer .btn-close").click(function () {
        $(".popup-outer .tab-item.active").removeClass("active");
        $(".popup-outer .tab-link.active").removeClass("active");
        $(".popup-outer").fadeOut(300);
        $(".popup-outer").removeClass("active");
    });

    $(".filter-arae .filter-item .label").click(function () {
        var $this = $(this);
        var $list = $this.closest(".filter-item").find("ul");
        if (!$this.hasClass("active")) {
            $(".filter-arae .filter-item ul.active").slideUp(300);
            $(".filter-arae .filter-item ul.active").removeClass("active");
            $(".filter-arae .filter-item .label").removeClass("active");
            $this.addClass("active");
            $list.addClass("active");
            $list.slideDown(300);
        }
        else {
            $this.removeClass("active");
            $list.slideUp(300);
            $list.removeClass("active");
        }
    });

    if ($(".input-field .current").length > 0) {
        $(".input-field .current").click(function () {
            var $this = $(this);
            var $list = $this.closest(".input-field").find(".list");
            if (!$this.hasClass("active")) {
                $(".input-field .current.active").closest(".input-field").find(".list").slideUp(300).removeClass("active");
                $(".input-field .current.active").removeClass("active");
                $this.addClass("active");
                $list.addClass("active");
                $list.slideDown(300);
            }
            else {
                $list.slideUp(300);
                $this.removeClass("active");
                $list.removeClass("active");
            }
        });

        $(".list").on("click", "div:not(.add-option)", function () {
            var $this = $(this);
            var $currentTxt = $this.closest(".input-field").find(".current.active");
            var $list = $this.closest(".input-field").find(".list.active");
            if (!$this.hasClass("selected")) {
                $(".list.active div.selected").removeClass("selected");
                $this.addClass("selected");
                $currentTxt.html($this.html()).removeClass("active");
                $list.slideUp(300).removeClass("active");
            }
        });
    }

    $("header .lng-area .current a").click(function () {
        var $this = $(this);
        var $otherLng = $this.closest(".lng-area").find(".other");
        if (!$this.hasClass("active")) {
            $this.addClass("active");
            $otherLng.slideDown(300).addClass("active");
        }
        else {
            $otherLng.slideUp(300).removeClass("active");
            $this.removeClass("active");
        }
    })

    $(".match-detail-page.history .match-detail .button-area .buttons #btnDispute").click(function () {
        var $this = $(this);
        $this.closest("body").find("#disputePopup").addClass("active").fadeIn(300);
    });

    if ($("main").hasClass("create-match-page")) {

        $(".input-field .add-option .btn-add").click(function (event) {
            event.preventDefault();
            var $this = $(this);
            var $input = $(".input-field .add-option .add-input");
            if ($input.val().trim() !== "") {
                $this.closest(".list").prepend("<div>" + $input.val() + "</div>");
                $input.val("")
            }
        });

        $("#btnCreateMatch").click(function (event) {
            event.preventDefault();
            // $(".validation-error-msg").remove();
            // $(".input-field").each(function () {
            //     var $this = $(this);
            //     if ($this.closest(".current").text() == "") {
            //         $this.append("<div class='validation-error-msg'>This field is required</div>")
            //     }
            // });
            $("#createMatchSuccessNotify").addClass("active").fadeIn(300);
            setTimeout(function () {
                $("#createMatchSuccessNotify").fadeOut(300).removeClass("active");
            }, 2000)
        });

        $('.datepicker-here').datepicker({
            language: 'en'
        });
    }
    $("footer .footer-navigation .item[data-href='questions']").click(function () {
        $("html, bocy").animate({ scrollTop: $("section.questions-section").offset().top - $("header").outerHeight() })
    });

    if ($("main").hasClass("browse-match-page")) {
        if ($("header .buttons-area.login.d-none").length === 0) {
            $(".questions-section").addClass("d-none");
            $(".about-section").addClass("d-none");
            $(".gamer-dashboard-bg.d-none").removeClass("d-none");
            $(".browse-match-section").addClass("is-logged");
        }
        else {
            $(".cards-list-area>.button-area.d-none").removeClass("d-none");
            $(".section-title.d-none").removeClass("d-none");
            $(".browse-match-section .header-img.d-none").removeClass("d-none");
            $(".questions-section.d-none").removeClass("d-none");
            $(".about-section.d-none").removeClass("d-none");
            $(".browse-match-section.is-logged").removeClass("is-logged");
        }
    }

    $(document).on("click", function (e) {
        if ($(".left-navigation").hasClass("open") && $(e.target).closest(".left-navigation.open").length === 0 && $(e.target).closest(".menu-btn").length === 0) {
            $("header .menu-btn").removeClass("active");
            $(".left-navigation").removeClass("open");
            $("header").removeClass("active");
            $(".logo-area .small-logo").removeClass("d-none");
            $(".logo-area .big-logo").addClass("d-none");
            $(".left-navigation .games-list .item .name").fadeOut(0);
            $(".left-navigation .navigation ul").addClass("d-none");
            $(".left-navigation  .bottom-info .info-inner").addClass("d-none");
            $("header .layer").addClass("d-none");
        }
        if ($(".popup-outer").hasClass("active") && $(e.target).closest(".popup-content").length === 0 && $(e.target).closest("header .buttons-area").length === 0 && $(e.target).closest(".sign-btn").length === 0) {
            $(".popup-outer .tab-item.active").removeClass("active");
            $(".popup-outer .tab-link.active").removeClass("active");
            $(".popup-outer").fadeOut(300);
            $(".popup-outer").removeClass("active");
        }
        if ($(".filter-arae .filter-item .label").hasClass("active") && $(e.target).closest(".filter-arae .filter-item").length === 0 && $(e.target).closest(".filter-arae .filter-item ul").length === 0) {
            $(".filter-arae .filter-item ul.active").slideUp(300);
            $(".filter-arae .filter-item ul.active").removeClass("active");
            $(".filter-arae .filter-item .label").removeClass("active");
        }
        if ($("header .buttons-area .user-info-area .user-info").hasClass("active") && $(e.target).closest(".user-info-area .user-info").length === 0 && $(e.target).closest(".user-info-area .username-btn").length === 0) {
            $("header .buttons-area .user-info-area .triangle").fadeOut(300);
            $("header .buttons-area .user-info-area .user-info.active").removeClass("active").fadeOut(300);
            $("header .buttons-area .user-info-area .username-btn").removeClass("active");
        }
        if ($("header .buttons-area .notifications").hasClass("active") && $(e.target).closest(".notification-area .notifications").length === 0 && $(e.target).closest(".notification-area .notification-btn").length === 0 && $(e.target).closest(".notification-area .notification").length === 0) {
            $("header .buttons-area .triangle").fadeOut(300);
            $("header .buttons-area .notifications.active").removeClass("active").fadeOut(300);
            $("header .buttons-area .notification-btn.active").removeClass("active");
            $("header .buttons-area .notification-area .n-list .item.active .notification.active").fadeOut(300).removeClass("active");
            $("header .buttons-area .notification-area .n-list .item.active").removeClass("active");
        }
        if ($(e.target).closest("form .input-field .current.active").length === 0 && $(e.target).closest("form .input-field .list.active").length === 0) {
            $("form .input-field .current.active").removeClass("active");
            $("form .input-field .list.active").slideUp(300).removeClass("active");
        }
        if ($(e.target).closest(".popup-dispute").length === 0 && $(e.target).closest("#btnDispute").length === 0) {
            $("#disputePopup").fadeOut(300).removeClass("active");
        }
        if ($(e.target).closest(".lng-area").length === 0 && $(e.target).closest(".lng-area .current").length === 0) {
            $(".lng-area .other.active").slideUp(300).removeClass("active");
            $(".lng-area .current a.active").removeClass("active");
        }
    });
})


// Tournament


$('.one-tournament-area .tab-header').on('click', 'a:not(.active)', function(){
    var $this = $(this);
    $('.one-tournament-area .tabs-area .tab-header a.active').removeClass('active');
    $this.addClass('active');
    $('.one-tournament-area .tabs-area .tab-content>.item.active').removeClass('active');
    $('.one-tournament-area .tabs-area .tab-content>.item[data-tab="' + $this.attr('data-tab') + '"]').addClass('active');
});
$(".rules-list .item .rule-tab a").click(function () {
    var $item = $(this).closest(".item");
    var $text = $item.find(".tab-content");
    if (!$item.hasClass("active")) {
        $(".rules-list .item.active .tab-content").slideUp(300);
        $(".rules-list .item.active").removeClass("active");
        $item.addClass("active");
        $text.slideDown(300);
    }
    else {
        $text.slideUp(300);
        $item.removeClass("active");
    }
});
