var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Proschall' });
});
router.get('/browse-game-matches', function (req, res, next) {
  res.render('browseGameMatches', { title: 'Proschall' });
});
router.get('/match-details', function (req, res, next) {
  res.render('matchDetails', { title: 'Proschall' });
});
router.get('/my-chellenges', function (req, res, next) {
  res.render('myChellenges', { title: 'Proschall' });
});
router.get('/my-match-details', function (req, res, next) {
  res.render('mymatchDetails', { title: 'Proschall' });
});
router.get('/create-match', function (req, res, next) {
  res.render('createMatch', { title: 'Proschall' });
});
router.get('/history', function (req, res, next) {
  res.render('history', { title: 'Proschall' });
});
router.get('/history-match-detail', function (req, res, next) {
  res.render('historyMatchDetail', { title: 'Proschall' });
});
router.get('/profile', function (req, res, next) {
  res.render('profile', { title: 'Proschall' });
});
router.get('/account-security', function (req, res, next) {
  res.render('accountSecurity', { title: 'Proschall' });
});
router.get('/personal-detail', function (req, res, next) {
  res.render('personalDetail', { title: 'Proschall' });
});
router.get('/deposit', function (req, res, next) {
  res.render('deposit', { title: 'Proschall' });
});
router.get('/tournament', function (req, res, next) {
  res.render('tournament', { title: 'Proschall' });
});
router.get('/one-tournament', function (req, res, next) {
  res.render('one_tournament', { title: 'Proschall' });
});
module.exports = router;
